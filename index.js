const express = require('express')
require('./db')

const { getaccountaddress, getbalance } = require('./rpc')

const { Question, Answer } = require('./schema')

const PORT = 4000;
const app = express()
app.use(express.json())

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

app.get('/questions', async (req, res) => {
    Question.find((err, questions) => res.send(questions))
})

app.get('/questions-with-balances', async (req, res) => {
    let qs = await Question.find().lean().exec();
    let qa = qs.map(async q => ({ 
        ...q,
        bounty_amount: await getbalance(q._id)
    }))
    res.send(await Promise.all(qa))
})

app.get('/question/:id', async (req, res) => {
    Question.findById(req.params.id).exec()
        .then(q => res.send(q))
})

app.post('/question', async (req, res) => {
    let q = new Question({
        title: req.body.title,
        description: req.body.description,
    })
    q.bounty_address = await getaccountaddress(q._id);
    q.save()
})

app.post('/answer/:q_id', async (req, res) => {
    let answer = new Answer({
        content: req.body.content,
        reward_address: req.body.reward_address
    })

    let q = await Question.findById(req.params.q_id);
    q.answers.push(answer)
    q.save()
})

app.listen(PORT, () => console.log(`API available on port ${PORT}!`))