const mongoose = require('mongoose');
require('dotenv').config()


mongoose.connect(
    `mongodb+srv://db_user:${process.env.DB_PASSWORD}@answertrader-wkxmu.gcp.mongodb.net/test`,
    { useNewUrlParser: true }
)

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log("Connected to Atlas DB!")
});
