const jayson = require('jayson')
require('dotenv').config()

let { RPC_USER, RPC_PASSWORD, WALLET_ADDRESS, WALLET_PORT } = process.env;

const client = jayson.client.http(`http://${RPC_USER}:${RPC_PASSWORD}@${WALLET_ADDRESS}:${WALLET_PORT}`);

function rpc(command, args) {
    return new Promise((resolve, reject) => {
        client.request(command, args, (err, response) => {
            if (err) reject(err)
            else resolve(response.result)
        })
    })
}

module.exports = {
mnsync: () => rpc("mnsync", ['status']),
getinfo: () => rpc("getinfo", []),
getaccountaddress: (question_id) => rpc('getaccountaddress', [question_id]),
getbalance: (account) => rpc('getbalance', [account]),

}