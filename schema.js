let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let answer_schema = new Schema({
    id: String,
    content: String,
    reward_address: String
})
let question_schema = new Schema({
    id: String,
    title: String,
    description: String,
    bounty_address: String,
    answers: [answer_schema]
})

module.exports = {
    Question: mongoose.model('Question', question_schema),
    Answer: mongoose.model('Answer', answer_schema)
}